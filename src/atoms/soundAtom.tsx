import {atom} from 'recoil';

export const soundAtom = atom({
    key: 'muted',
    default: localStorage.getItem('muted') == "true"  || false
});
