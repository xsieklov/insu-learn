import FlippableCard from "../components/FlippableCard";
import {nextButton, skipToChapter, symptoms, symptomsCards, toChapter} from "../../public/lit/texts";
import {languageAtom} from "../atoms/languageAtom";
import {useRecoilValue} from "recoil";
import {Texts, useAnimate} from "../hooks/useAnimate";
import {useRef} from "react";
import Typewriter, {TypewriterClass} from "typewriter-effect";
import {useTranslate} from "../hooks/useTranslate";
import {useNavigate} from "react-router-dom";
import {AnimateCC} from "react-adobe-animate";
import {useAudioFiles} from "../hooks/useAudioFiles";

const Symptoms = () => {
    const language = useRecoilValue(languageAtom);
    const typewriterRef = useRef<TypewriterClass>();
    const nextButtonText = useTranslate(nextButton);
    const chapter4Text = useTranslate(toChapter) + "4";
    const skipToChapter4 = useTranslate(skipToChapter) + "4"
    const navigate = useNavigate();
    const audioFiles: string[] = useAudioFiles(12, "symptoms")

    const {onInit, handleClick, isAnimationComplete, isLast, getAnimationObject, paused, currentIndex} = useAnimate({
        typewriterRef,
        link: '/treatment',
        text: symptoms,
        audioFiles: audioFiles
    });
    return (
        <div className="flex">
            <div className="justify-center w-2/6 p-4 font-speech">
                <div id="intro-text">
                    <Typewriter
                        options={{
                            delay: 40,
                        }}
                        onInit={(typewriter) => onInit({typewriter, pauseFor: 0})}
                    />
                </div>
                <div className="fixed bottom-0 mb-2">
                    <div className="grid justify-center items-end">
                        <AnimateCC
                            animationName="symptoms"
                            getAnimationObject={getAnimationObject}
                            paused={paused}
                        />
                    </div>
                    <div className="flex justify-evenly">
                        <button
                            onClick={handleClick}
                            className={`border-4 border-black font-primary mr-8 ${
                                !isAnimationComplete ? 'bg-gray-main' : 'bg-yellow-main'
                            }`}
                        >
                            {isLast ? chapter4Text : nextButtonText}
                        </button>
                        {!isLast && (
                            <button
                                onClick={() => navigate("/treatment")}
                                className={`border-4 border-black font-primary bg-yellow-main`}
                            >
                                {skipToChapter4}
                            </button>
                        )}
                    </div>
                </div>
            </div>
            <div className="w-4/6 p-4">
                <div className="grid grid-rows-2 grid-cols-4 gap-4">
                    {symptomsCards.map((card, index) => (
                        <div
                            key={card.id}
                            className={`${
                                index < currentIndex - 2 ? "opacity-100" : "opacity-0 pointer-events-none"
                            } transition-opacity duration-1000`}
                        >
                            <FlippableCard
                                key={card.id}
                                image={card.image}
                                text={card.text[language as keyof Texts]}
                                alt={card.id}
                                currentIndex={currentIndex}
                                indexOfVisibility={index + 3}
                            />
                        </div>
                    ))}
                </div>
            </div>
        </div>
    );
}
export default Symptoms;
