import {useNavigate} from "react-router-dom";

const Chapters = () => {
    const navigate = useNavigate();

    const chapters = [
        {id: 'chpt1', label: '1', name: 'Introduction'},
        {id: 'chpt2', label: '2', name: 'Insulin'},
        {id: 'chpt3', label: '3', name: 'Symptoms'},
        {id: 'chpt4', label: '4', name: 'Treatment'},
    ];

    const handleChapterClick = (chapterId: string) => {
        switch (chapterId) {
            case 'chpt1':
                navigate('/intro');
                break;
            case 'chpt2':
                navigate('/insulin');
                break;
            case 'chpt3':
                navigate('/symptoms');
                break;
            case 'chpt4':
                navigate('/treatment');
                break;
            default:
                break;
        }
    };

    return (
        <>
            <div className="bg-primary-dark flex items-center justify-center"
                 style={{ minHeight: 'calc(100vh - 90px)'}} >
                {chapters.map((chapter, index) => (
                    <>
                        {index > 0 && (
                            <div className="line"></div>
                        )}
                        <div key={chapter.id}>
                            <div
                                id={chapter.id}
                                className="chapter cursor-pointer"
                                onClick={() => handleChapterClick(chapter.id)}
                            >
                                <div>{chapter.label}</div>
                                <div className="chapter-name">{chapter.name}</div>
                            </div>
                        </div>
                    </>
                ))}
            </div>
        </>
    );
};

export default Chapters;
