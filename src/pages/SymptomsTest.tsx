import {useEffect, useRef, useState} from "react";
import {
    correct,
    nextButton,
    stepperLabelsSymptoms,
    summaryHappy,
    summarySad,
    symptomsTestQuestions,
    symptomsText,
    treatmentText,
    yourScore
} from "../../public/lit/texts";
import {Stepper} from "react-stepa";
import {useRecoilValue} from "recoil";
import {languageAtom} from "../atoms/languageAtom";
import {Texts, useAnimate} from "../hooks/useAnimate";
import {useTranslate} from "../hooks/useTranslate";
import {useNavigate} from "react-router-dom";
import {AnimateCC} from "react-adobe-animate";
import Typewriter, {TypewriterClass} from "typewriter-effect";
import SymptomsDragNDropTest from "../components/SymptomsDragNDropTest";
import SymptomsCardTest from "../components/SymptomsCardTest";

const SymptomsTest = () => {
    const language = useRecoilValue(languageAtom);
    const [currentStep, setCurrentStep] = useState(0);
    const nextButtonText = useTranslate(nextButton);
    const correctButtonText = useTranslate(correct);
    const treatmentStepText = useTranslate(treatmentText);
    const symptomsStepText = useTranslate(symptomsText);
    const scoreText = useTranslate(yourScore);
    const [selectedAnswer, setSelectedAnswer] = useState<number | null>(null);
    const [isAnswered, setIsAnswered] = useState(false);
    const [score, setScore] = useState(0);
    const [isScoreBelow50, setIsScoreBelow50] = useState((score / 5) * 100 < 50);
    const navigate = useNavigate();
    const typewriterRef = useRef<TypewriterClass>();
    const {getAnimationObject} = useAnimate({
        typewriterRef,
        link: '/',
        text: [],
        audioFiles: []
    });

    useEffect(() => {
        setIsScoreBelow50((score / 7) * 100 < 50);
    }, [score]);

    const handleNextStep = () => {
        setCurrentStep(currentStep + 1);
        setSelectedAnswer(null);
        setIsAnswered(false);
    };

    const handleAnswerClick = (index: number) => {
        if (!isAnswered) {
            setSelectedAnswer(index);
            setIsAnswered(true);
            if (symptomsTestQuestions[currentStep].answers[index].isCorrect) {
                setScore(score + 1);
            }
        }
    };

    const generateSteps = () => {
        const updatedSteps = stepperLabelsSymptoms.map((question) => ({
            label: question[language as keyof Texts],
            description: "",
        }));

        updatedSteps.splice(updatedSteps.length - 1, 0, {label: `${treatmentStepText}`, description: ""});
        updatedSteps.splice(updatedSteps.length - 1, 0, {label: `${symptomsStepText}`, description: ""});
        return updatedSteps;
    };

    const isDraggableTestStep = currentStep === 5;
    const isSymptomTest = currentStep === 6;
    const isSummaryStep = currentStep === 7;

    return (
        <div className="overflow-auto mx-auto py-8">
            <Stepper
                customStyle={{
                    completed: '#3D348B',
                    pending: '#aaccff',
                    progress: '#5599ff',
                }}
                steps={generateSteps()}
                activeStep={currentStep}
            />
            <div className="text-center mt-4">
                {isSummaryStep ? (
                    <>
                        <p className="text-xl font-semibold">
                            {`${scoreText} ${(score / 7 * 100).toFixed(0)}%`}
                        </p>
                        {isScoreBelow50 ? (
                            <div id="intro-text" className="font-speech">
                                <Typewriter
                                    options={{
                                        delay: 40,
                                    }}
                                    onInit={(typewriter) => {
                                        typewriter.typeString(summarySad[language as keyof Texts])
                                        typewriter.start()
                                    }}
                                />
                            </div>
                        ) : (
                            <div id="intro-text" className="font-speech text-center">
                                <Typewriter
                                    options={{
                                        delay: 40,
                                    }}
                                    onInit={(typewriter) => {
                                        typewriter.typeString(summaryHappy[language as keyof Texts])
                                        typewriter.start()
                                    }}
                                />
                            </div>
                        )}
                        <div className="w-[300px] max-h-[510px] h-auto justify-center inline-block">
                            {isScoreBelow50 ? (
                                <div className="w-[300px] max-h-[510px] mb-4 h-auto">
                                    <AnimateCC
                                        animationName="sad"
                                        getAnimationObject={getAnimationObject}
                                    />
                                </div>
                            ) : (
                                <div className="w-[300px] max-h-[510px] mb-4 h-auto">
                                    <AnimateCC
                                        animationName="happy"
                                        getAnimationObject={getAnimationObject}
                                    />
                                </div>
                            )}
                        </div>
                    </>
                ) : isDraggableTestStep ? (
                    <></>
                ) : isSymptomTest ? (
                    <></>
                ) : (
                    <p className="text-xl py-4">
                        {language === "EN"
                            ? symptomsTestQuestions[currentStep].EN
                            : symptomsTestQuestions[currentStep].SK}
                    </p>
                )}
            </div>
            {isSummaryStep ? (
                <button
                    className="bg-primary-light hover:bg-primary-main text-white font-bold py-2 mt-4 px-4 rounded"
                    onClick={() => navigate("/")}
                >
                    {nextButtonText}
                </button>
            ) : isDraggableTestStep ? (
                <SymptomsDragNDropTest handleNext={handleNextStep} setScore={setScore} score={score}/>
            ) : isSymptomTest ? (
                <SymptomsCardTest handleNext={handleNextStep} setScore={setScore} score={score}></SymptomsCardTest>
            ) : (
                <div>
                    <div>
                        <div className="ml-4 text-xl">
                            {symptomsTestQuestions[currentStep].answers.map((answer, index) => (
                                <button
                                    key={index}
                                    className={` ${
                                        selectedAnswer === index
                                            ? answer.isCorrect
                                                ? "bg-green-main"
                                                : "bg-red-500"
                                            : "bg-primary-dark"
                                    } ${
                                        selectedAnswer !== null ? "cursor-not-allowed" : ""
                                    } hover:bg-primary-light py-2 px-4 rounded mr-4 mt-4`}
                                    onClick={() => handleAnswerClick(index)}
                                    disabled={isAnswered}
                                >
                                    {language === "EN" ? answer.EN : answer.SK}
                                </button>
                            ))}
                        </div>
                        {selectedAnswer !== null && (
                            <div
                                className={`text-${symptomsTestQuestions[currentStep]
                                    .answers[selectedAnswer].isCorrect ? "green" : "red"}-500 mt-4 text-xl`}>
                                {symptomsTestQuestions[currentStep].answers[selectedAnswer].isCorrect
                                    ? `${correctButtonText}`
                                    : symptomsTestQuestions[currentStep].explanation[language as keyof Texts]}
                            </div>
                        )}
                        {!isSummaryStep && (
                            <button
                                className={`${!isAnswered ? 'bg-gray-main' : 'bg-primary-light  hover:bg-primary-main'} text-white font-bold mt-4 py-2 px-4 rounded`}
                                onClick={handleNextStep}
                                disabled={!isAnswered}
                            >
                                {nextButtonText}
                            </button>
                        )}
                    </div>
                    <div className="w-96 h-96 mt-8 inline-block border-sky-500 border-4 border-double">
                        <AnimateCC
                            animationName={`symptomsTest_q${currentStep + 1}`}
                            getAnimationObject={getAnimationObject}
                        />
                    </div>
                </div>
            )}
        </div>
    );
};
export default SymptomsTest;
