import {useRef} from 'react';
import {AnimateCC} from 'react-adobe-animate';
import Typewriter, {TypewriterClass} from "typewriter-effect";
import {intro, nextButton, skipToChapter, toChapter} from "../../public/lit/texts";
import {useAnimate} from "../hooks/useAnimate";
import {useTranslate} from "../hooks/useTranslate";
import {useNavigate} from "react-router-dom";
import {useAudioFiles} from "../hooks/useAudioFiles";

const Intro = () => {
    const nextButtonText = useTranslate(nextButton);
    const chapter1Text = useTranslate(toChapter) + "1";
    const typewriterRef = useRef<TypewriterClass>();
    const navigate = useNavigate();
    const skipButtonText = useTranslate(skipToChapter) + "1";

    const audioFiles: string[] = useAudioFiles(10, "intro")

    const {getAnimationObject, handleClick, isAnimationComplete, isLast, onInit, paused} = useAnimate({
        typewriterRef,
        link: '/insulin',
        text: intro,
        audioFiles: audioFiles
    });

    return (
        <>
            <div className="flex items-center justify-center">
                <div className="container">
                    <div className="text mt-12">
                        <div id="intro-text" className="font-speech">
                            <Typewriter
                                options={{
                                    delay: 40,
                                }}
                                onInit={(typewriter) => onInit({typewriter, pauseFor: 0})}
                            />
                        </div>
                        <div className="mt-12 ml-44 flex justify-center">
                            <div
                                style={{width: '600px', maxWidth: '600px', height: '300px', position: 'relative'}}
                            >
                                <AnimateCC
                                    animationName="intro"
                                    getAnimationObject={getAnimationObject}
                                    paused={paused}
                                />
                            </div>
                        </div>
                    </div>
                    <div className="mt-20 flex justify-evenly">
                        <button
                            onClick={handleClick}
                            className={`mt-20 border-4 border-black font-primary ${
                                !isAnimationComplete ? 'bg-gray-main' : 'bg-yellow-main'
                            }`}
                        >
                            {isLast ? chapter1Text : nextButtonText}
                        </button>
                        {!isLast && (
                        <button
                            onClick={() => navigate("/insulin")}
                            className={`mt-20 border-4 border-black font-primary bg-yellow-main`}
                        >
                            {skipButtonText}
                        </button>
                            )}
                    </div>
                </div>
            </div>
        </>
    );
};
export default Intro;
