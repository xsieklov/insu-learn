import {useEffect, useRef, useState} from 'react';
import {AnimateCC} from 'react-adobe-animate';
import Typewriter, {TypewriterClass} from "typewriter-effect";
import {glucoseExplanation, insulinExplanation, nextButton, opening, skipToTest, toTest} from "../../public/lit/texts";
import {useAnimate} from "../hooks/useAnimate";
import {useTranslate} from "../hooks/useTranslate";
import {useNavigate} from "react-router-dom";
import ExplanationPopup from "../components/ExplanationPopup";
import {useRecoilValue} from "recoil";
import {languageAtom} from "../atoms/languageAtom";
import {useAudioFiles} from "../hooks/useAudioFiles";

const Insulin = () => {
    const nextButtonText = useTranslate(nextButton);
    const testText = useTranslate(toTest);
    const typewriterRef = useRef<TypewriterClass>();
    const skipButtonText = useTranslate(skipToTest);
    const insulinExplanationText = useTranslate(insulinExplanation);
    const glucoseExplanationText = useTranslate(glucoseExplanation);
    const navigate = useNavigate();
    const [isPopupOpen, setIsPopupOpen] = useState(false);
    const [isAnimPopupOpen, setIsAnimPopupOpen] = useState(false);
    const language = useRecoilValue(languageAtom);
    const audioFiles: string[] = useAudioFiles(13, "insulin")

    const {
        getAnimationObject, handleClick, isAnimationComplete, isLast, onInit,
        paused, animationObj, currentIndex
    } = useAnimate({
        typewriterRef,
        link: '/insulinTest',
        text: opening,
        audioFiles: audioFiles
    });

    useEffect(() => {
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        getAnimationObject((prev: any) => {
            if (prev === undefined || prev === null) return prev;
            const glucoseInstance = prev["glucose"];
            console.log("glucoseInstance:", glucoseInstance);

            if (glucoseInstance) {
                if (!glucoseInstance._listeners?.click) {
                    glucoseInstance.on("click", () => {
                        setIsAnimPopupOpen((prev) => !prev);
                    });

                }
            }
            prev["glucose"] = glucoseInstance;
            return prev;
        });
    }, [animationObj, getAnimationObject, isAnimPopupOpen]);

    return (
        <>
            <div className="mt-8">
                <p
                    className="text-2xl bg-red-200 font-primary border border-red-800 w-max justify-self-center inline p-4 animate-fade-in"
                >
                    {language === 'EN' ? (
                        <>
                            Diabetes is a disease in which the body is unable to produce a sufficient amount of&nbsp;
                            <span
                                className="clickable-text"
                                onClick={() => setIsPopupOpen(!isPopupOpen)}
                                style={{textDecoration: 'underline', cursor: 'pointer'}}
                            >
                                insulin.
                            </span>
                        </>
                    ) : (
                        <>
                            Cukrovka je ochorenie, pri ktorom telo nedokáže produkovať dostatočné množstvo&nbsp;
                            <span
                                className="clickable-text"
                                onClick={() => setIsPopupOpen(!isPopupOpen)}
                                style={{textDecoration: 'underline', cursor: 'pointer'}}
                            >
                                inzulínu.
                            </span>
                        </>
                    )}
                </p>
            </div>

            <div className="flex items-center">
                <div className="w-[100%]">
                    <div className="text mt-8">
                        <div className="flex relative">
                            <div id="animation-container">
                                <div id="intro-text" className="font-speech max-w-md absolute top-[15%] left-[7%]">
                                    <Typewriter
                                        options={{
                                            delay: 40,
                                        }}
                                        onInit={(typewriter) => onInit({typewriter, pauseFor: 3400})}
                                    />
                                </div>
                                <div>
                                    {isPopupOpen && (
                                        <ExplanationPopup
                                            className="absolute top-5 right-5 bg-white p-4 border border-gray-300 rounded shadow-lg max-w-xs z-10 flex"
                                            explanation={insulinExplanationText}
                                        />
                                    )}
                                    {isAnimPopupOpen && currentIndex < 4 && (
                                        <ExplanationPopup
                                            className="absolute top-64 right-15 right-4 bg-white p-4 border border-gray-300 rounded shadow-lg max-w-xs z-10 flex"
                                            explanation={glucoseExplanationText}
                                        />
                                    )}
                                </div>
                                <AnimateCC
                                    animationName="opening"
                                    getAnimationObject={getAnimationObject}
                                    paused={paused}
                                />
                            </div>
                        </div>
                    </div>

                    <div className="flex justify-evenly">
                        <button
                            onClick={handleClick}
                            className={`border-4 border-black font-primary ${
                                !isAnimationComplete ? 'bg-gray-main' : 'bg-yellow-main'
                            }`}
                        >
                            {isLast ? testText : nextButtonText}
                        </button>
                        <button
                            onClick={() => navigate("/insulinTest")}
                            className={`border-4 border-black font-primary bg-yellow-main`}
                        >
                            {skipButtonText}
                        </button>
                    </div>
                </div>
            </div>
        </>
    );
};
export default Insulin;
