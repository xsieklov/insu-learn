import {AnimateCC} from "react-adobe-animate";
import {Texts, useAnimate} from "../hooks/useAnimate";
import {nextButton, skipToTest, toTest, treatment, treatmentOptions} from "../../public/lit/texts";
import {useEffect, useRef, useState} from "react";
import Typewriter, {TypewriterClass} from "typewriter-effect";
import {useNavigate} from "react-router-dom";
import {useTranslate} from "../hooks/useTranslate";
import {useRecoilValue} from "recoil";
import {languageAtom} from "../atoms/languageAtom";
import {useAudioFiles} from "../hooks/useAudioFiles";

const Treatment = () => {
    const typewriterRef = useRef<TypewriterClass>();
    const navigate = useNavigate();
    const toTestText = useTranslate(toTest);
    const skipToTestText = useTranslate(skipToTest);
    const nextButtonText = useTranslate(nextButton);
    const language = useRecoilValue(languageAtom);
    const [selectedInstance, setSelectedInstance] = useState('');
    const audioFiles: string[] = useAudioFiles(12, "treatment")

    const {onInit, handleClick, isAnimationComplete, isLast, getAnimationObject, paused, animationObj} = useAnimate({
        typewriterRef,
        link: '/symptomsTest',
        text: treatment,
        audioFiles: audioFiles
    });

    useEffect(() => {
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        getAnimationObject((prev: any) => {
            if (prev === undefined || prev === null) return prev;
            const instanceNames = ["glucometer", "water", "diet", "pills", "insulinPump", "insulin", "sport"];
            instanceNames.forEach((instanceName) => {
                const instance = prev[instanceName];
                if (instance) {
                    if (!instance._listeners?.click) {
                        instance.on("click", () => {
                            setSelectedInstance(() => instanceName);
                            console.log(selectedInstance);
                        });
                    }
                    prev[instanceName] = instance;
                }
            });
            return prev;
        });
    }, [animationObj, getAnimationObject, selectedInstance]);

    return (
        <div className="flex">
            <div className="justify-center w-1/4 p-4 font-speech">
                <div id="intro-text">
                    <Typewriter
                        options={{
                            delay: 40,
                        }}
                        onInit={(typewriter) => onInit({typewriter, pauseFor: 0})}
                    />
                </div>
                <div className="fixed bottom-0 mb-2 w-[23rem]">
                    <div className="grid justify-center items-end">
                        <AnimateCC
                            animationName="treatment_blo"
                            getAnimationObject={getAnimationObject}
                            paused={paused}
                        />
                    </div>
                    <div className="flex justify-evenly">
                        <button
                            onClick={handleClick}
                            className={`border-4 border-black font-primary mr-8 ${
                                !isAnimationComplete ? 'bg-gray-main' : 'bg-yellow-main'
                            }`}
                        >
                            {isLast ? toTestText : nextButtonText}
                        </button>
                        {!isLast && (
                            <button
                                onClick={() => navigate("/symptomsTest")}
                                className={`border-4 border-black font-primary bg-yellow-main`}
                            >
                                {skipToTestText}
                            </button>
                        )}
                    </div>
                </div>
            </div>
            <div className="w-1/2 p-4 justify-center grid">
                <div className="w-[48rem]">
                    <AnimateCC
                        animationName="treatment"
                        getAnimationObject={getAnimationObject}
                        paused={paused}
                    />
                </div>
            </div>
            {selectedInstance != "" && (
                <div className="w-1/4 p-4">
                    <div className="bg-primary-dark rounded-lg p-4 mb-4 border border-primary-light">
                        <div className="font-bold text-xl mb-2">
                            {treatmentOptions.find(option => option.id === selectedInstance)?.title[language as keyof Texts]}
                        </div>
                    </div>
                    <div className="bg-primary-dark rounded-lg p-4 mb-4 border border-primary-light">
                        <p className="text-md fontt leading-6">{treatmentOptions.find(option => option.id === selectedInstance)?.text[language as keyof Texts]}</p>
                    </div>
                </div>
            )}
        </div>
    )
}
export default Treatment;
