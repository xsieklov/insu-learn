import {useNavigate} from "react-router-dom";
import {useState} from "react";
import {useRecoilState} from "recoil";
import {languageAtom} from "../atoms/languageAtom";
import {startButton} from "../../public/lit/texts";
import {Texts} from "../hooks/useAnimate";

const Intro = () => {
    const navigate = useNavigate();
    const [showLangMenu, setShowLangMenu] = useState(false);
    const [language, setLanguage] = useRecoilState(languageAtom);
    const startButtonText = startButton[language as keyof Texts]

    const handleLanguageChange = (newLanguage: string) => {
        localStorage.setItem('selectedLanguage', newLanguage);
        setLanguage(newLanguage);
        setShowLangMenu(false);
    };
    const handleClick = () => {
        navigate('/intro');
    }

    return (
        <>
            <button
                onClick={handleClick}
                className="mt-20 border-2 border-black font-primary bg-primary-light z-10 absolute top-28 left-28">
                {startButtonText}
            </button>
            <button
                className="mt-20 border-2 border-black font-primary bg-primary-light z-10 absolute top-44 left-28"
                onClick={() => setShowLangMenu(!showLangMenu)}
            >
                {language === 'EN' ? 'English' : 'Slovenčina'}
            </button>
            {showLangMenu && (
                <div
                    className="mt-2 p-2 border-4 border-black font-primary bg-primary-main z-10 absolute top-60 left-28">
                    <button
                        className="block mt-1 border-2 border-black font-primary bg-primary-light hover:bg-primary-dark"
                        onClick={() => handleLanguageChange('EN')}
                    >
                        English
                    </button>
                    <button
                        className="block mt-1 border-2 border-black font-primary bg-primary-light hover:bg-primary-dark"
                        onClick={() => handleLanguageChange('SK')}
                    >
                        Slovenčina
                    </button>
                </div>
            )}
            <div className="bg-primary-dark w-[100%] flex flex-col-reverse items-center absolute
                            bottom-0 left-0 z-3"
                 style={{ minHeight: 'calc(100vh - 90px)' }}>
                <div className="bg-primary-dark">
                    <img
                        src="public/backgrounds/homepage-static.svg"
                        alt="background picture"
                        className="max-h-[40rem] mx-auto"/>
                </div>
            </div>
        </>
    );
};
export default Intro;
