import {Link, useNavigate} from 'react-router-dom';
import menuIcon from '../../public/icons/menu-icon.svg';
import logo from '../../public/lit/logo.svg';
import {useState} from 'react';
import volumeDown from "../../public/icons/volume_off_black.svg";
import volumeUp from "../../public/icons/volume_up_black.svg";
import {useRecoilState} from "recoil";
import {soundAtom} from "../atoms/soundAtom";

const Navbar = () => {
    const navigate = useNavigate();
    const [muted, setMuted] = useRecoilState(soundAtom);
    const [levelsShown, setLevelsShown] = useState(false);

    const handleMuteToggle = () => {
        const newMutedState = !muted;
        localStorage.setItem('muted', `${newMutedState}`);
        setMuted(newMutedState);
    };

    return (
        <nav className="bg-primary-main z-10 relative w-[100%]">
            <div className="p-2 flex justify-between">
                <div>
                    <img src={logo} alt="Logo"
                         onClick={() => navigate("/")}
                         className="w-52 mt-4 ml-4 hover:cursor-pointer"/>
                </div>
                <div className="flex">
                    <div>
                        {muted ? (
                            <img
                                onClick={handleMuteToggle}
                                src={volumeDown} alt="Volume off"/>
                        ) : (
                            <img
                                onClick={handleMuteToggle}
                                src={volumeUp} alt="Volume On"/>
                        )}
                    </div>
                    <div className="flex items-center justify-end ml-2">
                        <Link to="/chapters">
                            <img src={menuIcon} onClick={() => {
                                setLevelsShown(!levelsShown);
                                const page = levelsShown ? '/intro' : '/chapters';
                                navigate(page);
                            }}
                                 alt="Chapters Icon"/>
                        </Link>
                    </div>
                </div>
            </div>
        </nav>
    )

};

export default Navbar;
