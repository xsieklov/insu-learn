import {useRecoilValue} from "recoil";
import {languageAtom} from "../atoms/languageAtom";

export const useAudioFiles = (endIndex: number, chapter: string): string[] => {
    const language = useRecoilValue(languageAtom);

    const audioFiles: string[] = [];
    for (let i = 1; i <= endIndex; i++) {
        const filePath = `public/sounds/${chapter}/${language}_${chapter}_${i}.mp3`;
        audioFiles.push(filePath);
    }
    return audioFiles;
}
