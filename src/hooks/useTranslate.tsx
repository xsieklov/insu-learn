import {useRecoilValue} from "recoil";
import {languageAtom} from "../atoms/languageAtom";
import {Texts} from "./useAnimate";

export const useTranslate = ( textLine: {EN: string, SK: string}) => {
    const language = useRecoilValue(languageAtom);
    return textLine[language as keyof Texts];
}
