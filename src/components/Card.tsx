export interface CardProps {
    image: string,
    text: string,
    alt: string,
    isSelected: boolean,
    onClick: () => void
}

function Card(props: CardProps) {
    return (
        <div
            className={`border-sky-500 border-4 border-sky-500 rounded-md relative hover:cursor-pointer
             ${props.isSelected ? 'bg-primary-light' : ''}`}
            onClick={props.onClick}
        >
            <div className="w-full h-full flex flex-col items-center justify-between align-center">
                <div className="flex flex-col justify-center flex-1">
                <img src={props.image} alt={props.alt} className="w-52 h-68 p-4 object-cover"/>
                </div>
                <p>{props.text}</p>
            </div>
        </div>
    );
}

export default Card;
