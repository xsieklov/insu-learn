import {DragDropContext, Draggable, DraggableProvided, DroppableProvided, DropResult} from 'react-beautiful-dnd';
import {StrictModeDroppable} from "./StrictModeDroppable";
import {Dispatch, SetStateAction, useEffect, useState} from "react";
import {Texts} from "../hooks/useAnimate";
import {useRecoilValue} from "recoil";
import {languageAtom} from "../atoms/languageAtom";
import {correct, glucoseTimeline, InsulinDragDropTitle, nextButton} from "../../public/lit/texts";
import {useTranslate} from "../hooks/useTranslate";
import {useShuffle} from "../hooks/useShuffle";

interface InsulinDragTestProps {
    handleNext: () => void,
    setScore: Dispatch<SetStateAction<number>>,
    score: number
}

const InsulinDragNDropTest = ({handleNext, setScore, score}: InsulinDragTestProps) => {
    const title = useTranslate(InsulinDragDropTitle);
    const correctText = useTranslate(correct);
    const nextText = useTranslate(nextButton);
    const initialOrder = useShuffle([...glucoseTimeline]);
    const [timeline, setTimeline] = useState(initialOrder);
    const language = useRecoilValue(languageAtom);
    const [isCorrectOrder, setIsCorrectOrder] = useState(false);

    const handleOnDragEnd = (result: DropResult) => {
        console.log(result);
        if (!result.destination) {
            return;
        }
        const items = Array.from(timeline);
        const [reorderedItem] = items.splice(result.source.index, 1);
        items.splice(result.destination.index, 0, reorderedItem);
        setTimeline(items);
    }

    useEffect(() => {
        const isOrderCorrect = timeline.every((item, index) => item.id === glucoseTimeline[index].id);
        setIsCorrectOrder(isOrderCorrect);
    }, [timeline]);

    const handleNextClick = () => {
        setScore(score + 1);
        handleNext();
    };

    return (
        <>
            <p className="text-lg align-center block mb-4">{title}</p>
            {isCorrectOrder &&
                <p className="max-w-[500px] align-center mx-auto my-4 bg-green-main text-xl">{correctText}</p>
            }
            <div className="w-1/3 inline-block">
                {isCorrectOrder && <button className="border-4 border-black font-primary bg-yellow-main mb-4"
                                           onClick={handleNextClick}>{nextText}</button>}
                <DragDropContext onDragEnd={handleOnDragEnd}>
                    <StrictModeDroppable droppableId="glucoseTimeline">
                        {(provided: DroppableProvided) => (
                            <ul className={`glucoseTimeline ${isCorrectOrder ? 'correct' : ''}`} {...provided.droppableProps}
                                ref={provided.innerRef}>
                                {timeline.map(({id, text, image}, index) => {
                                    return (
                                        <Draggable key={id} draggableId={id} index={index}>
                                            {(provided: DraggableProvided) => (
                                                <li ref={provided.innerRef} {...provided.draggableProps} {...provided.dragHandleProps}>
                                                    <div className="timeline-image">
                                                        <img src={image}
                                                             alt={`${text[language as keyof Texts]} image`}/>
                                                    </div>
                                                    <p className="text-left">
                                                        {text[language as keyof Texts]}
                                                    </p>
                                                </li>
                                            )}
                                        </Draggable>
                                    );
                                })}
                                {provided.placeholder}
                            </ul>
                        )}
                    </StrictModeDroppable>
                </DragDropContext>
            </div>
        </>
    )
}

export default InsulinDragNDropTest;
