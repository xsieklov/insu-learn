import {useState} from 'react';

interface CardFlipProps {
    image: string,
    text: string,
    alt: string,
    indexOfVisibility: number,
    currentIndex: number
}

function FlippableCard(props: CardFlipProps) {
    const [isFlipped, setIsFlipped] = useState(false);

    const handleCardClick = () => {
        if (props.currentIndex >= props.indexOfVisibility) {
            setIsFlipped(!isFlipped);
        }
    };

    return (
        <div
            className={`w-64 h-96 border-sky-500 border-4 rounded-md relative hover:cursor-pointer card ${isFlipped ? 'flipped' : ''}`}
            onClick={handleCardClick}
        >
            <div className="front w-full h-full flex items-center justify-center">
                <img src={props.image} alt={props.alt} className="w-52 h-68 p-4 object-cover"/>
            </div>
            <div className="back bg-primary-dark w-full h-full flex items-center justify-center">
                <p>{props.text}</p>
            </div>
        </div>
    );
}

export default FlippableCard;
