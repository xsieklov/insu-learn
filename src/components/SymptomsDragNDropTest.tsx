import {DragDropContext, Draggable, DraggableProvided, DroppableProvided, DropResult} from 'react-beautiful-dnd';
import {StrictModeDroppable} from "./StrictModeDroppable";
import {Dispatch, SetStateAction, useEffect, useState} from "react";
import {Texts} from "../hooks/useAnimate";
import {useRecoilValue} from "recoil";
import {languageAtom} from "../atoms/languageAtom";
import {
    correct,
    helpfulTools,
    nextButton,
    treatmentCards,
    TreatmentDragDropTitle,
    unhelpfulTools
} from "../../public/lit/texts";
import {useTranslate} from "../hooks/useTranslate";

interface Item {
    id: string;
    text: {
        EN: string;
        SK: string;
    };
    image: string;
}

interface SymptomsDragTestProps {
    handleNext: () => void,
    setScore: Dispatch<SetStateAction<number>>,
    score: number
}

const SymptomsDragNDropTest = ({handleNext, setScore, score}: SymptomsDragTestProps) => {
    const title = useTranslate(TreatmentDragDropTitle);
    const correctText = useTranslate(correct);
    const helpfulToolsText = useTranslate(helpfulTools);
    const unhelpfulToolsText = useTranslate(unhelpfulTools);
    const nextButtonText = useTranslate(nextButton);
    const [helpfulItems, setHelpfulItems] = useState<Item[]>([]);
    const [notHelpfulItems, setNotHelpfulItems] = useState<Item[]>([]);
    const [allItems, setAllItems] = useState(treatmentCards);
    const language = useRecoilValue(languageAtom);
    const [isAnswerCorrect, setIsAnswerCorrect] = useState(false);

    const handleOnDragEnd = (result: DropResult) => {
        if (!result.destination) {
            return;
        }
        let item: Item;
        if (result.source.droppableId === "helpfulZone") {
            item = helpfulItems[result.source.index]
        } else if (result.source.droppableId === "notHelpfulZone") {
            item = notHelpfulItems[result.source.index]
        } else {
            item = allItems[result.source.index]
        }

        const addItem = (prevItems: Item[]): Item[] => {
            const copy = [...prevItems];
            if (result.destination) {
                copy.splice(result.destination.index, 0, item);
            }
            return copy;
        }

        if (result.source.droppableId === "helpfulZone") {
            setHelpfulItems((prevItems) => prevItems.filter((item_) => item_.id !== item.id));
        } else if (result.source.droppableId === "notHelpfulZone") {
            setNotHelpfulItems((prevItems) => prevItems.filter((item_) => item_.id !== item.id));
        } else if (result.source.droppableId === "allItems") {
            setAllItems((prevItems) => prevItems.filter((item_) => item_.id !== item.id));
        }

        if (result.destination.droppableId === "helpfulZone") {
            setHelpfulItems(addItem);
        } else if (result.destination.droppableId === "notHelpfulZone") {
            setNotHelpfulItems(addItem);
        } else if (result.destination.droppableId === "allItems") {
            setAllItems(addItem);
        }
    };

    useEffect(() => {
        const helpfulItemsIds = new Set(helpfulItems.map(item => item.id));
        const notHelpfulItemsIds = new Set(notHelpfulItems.map(item => item.id));

        const isAnswerCorrect =
            helpfulItemsIds.size === 5 &&
            notHelpfulItemsIds.size === 5 &&
            [...helpfulItemsIds].every(id =>
                ['exerciseBloodSugar', 'balancedDiet', 'medicationRoutine',
                    'glucoseMonitoring', 'healthcareConsultation'].includes(id)) &&
            [...notHelpfulItemsIds].every(id =>
                ['avoidingMedication', 'ignoringBloodSugar', 'skippingMeals',
                    'neglectingExercise', 'ignoringHealthcareAdvice'].includes(id));

        setIsAnswerCorrect(isAnswerCorrect);
    }, [helpfulItems, notHelpfulItems, allItems]);

    const handleNextClick = () => {
        setScore(score + 1);
        handleNext();
    };

    return (
        <>
            <p className="text-xl align-center block">{title}</p>
            {isAnswerCorrect && (
                <p className="max-w-[500px] align-center mx-auto my-4 bg-green-main text-xl">{correctText}</p>
            )}
            {isAnswerCorrect && (
                <button className="border-4 border-black font-primary bg-yellow-main" onClick={handleNextClick}>
                    {nextButtonText}
                </button>
            )}
            <div className="flex justify-between mx-48">
                <h2 className="text-xl text-center mb-4">{helpfulToolsText}</h2>
                <h2 className="text-xl text-center mb-4">{unhelpfulToolsText}</h2>
            </div>
            <div className="w-full mt-4 flex">
                <DragDropContext onDragEnd={handleOnDragEnd}>
                    <StrictModeDroppable droppableId="helpfulZone" type="item" direction="vertical">
                        {(provided: DroppableProvided) => (
                            <>
                                <ul
                                    className={`glucoseTimeline w-1/3 mx-6 ${isAnswerCorrect ? 'correct' : ''}`}
                                    {...provided.droppableProps}
                                    ref={provided.innerRef}
                                >
                                    {helpfulItems.map(({id, text, image}, index) => (
                                        <Draggable key={id} draggableId={id} index={index}>
                                            {(provided: DraggableProvided) => (
                                                <li
                                                    ref={provided.innerRef}
                                                    {...provided.draggableProps}
                                                    {...provided.dragHandleProps}
                                                >
                                                    <div className="timeline-image">
                                                        <img src={image}
                                                             alt={`${text[language as keyof Texts]} image`}/>
                                                    </div>
                                                    <p className="text-left">{text[language as keyof Texts]}</p>
                                                </li>
                                            )}
                                        </Draggable>
                                    ))}
                                    {provided.placeholder}
                                </ul>
                            </>
                        )}
                    </StrictModeDroppable>
                    <StrictModeDroppable droppableId="allItems" type="item">
                        {(provided: DroppableProvided) => (
                            <ul
                                className={`glucoseTimeline w-1/3 ${isAnswerCorrect ? 'correct' : ''}`}
                                {...provided.droppableProps}
                                ref={provided.innerRef}
                            >
                                {allItems.map(({id, text, image}, index) => (
                                    <Draggable key={id} draggableId={id} index={index}>
                                        {(provided: DraggableProvided) => (
                                            <li
                                                ref={provided.innerRef}
                                                {...provided.draggableProps}
                                                {...provided.dragHandleProps}
                                            >
                                                <div className="timeline-image">
                                                    <img src={image}
                                                         alt={`${text[language as keyof Texts]} image`}/>
                                                </div>
                                                <p className="text-left">{text[language as keyof Texts]}</p>
                                            </li>
                                        )}
                                    </Draggable>
                                ))}
                                {provided.placeholder}
                            </ul>
                        )}
                    </StrictModeDroppable>
                    <StrictModeDroppable droppableId="notHelpfulZone" type="item" direction="vertical">
                        {(provided: DroppableProvided) => (
                            <>
                                <ul
                                    className={`glucoseTimeline w-1/3 mx-6 ${isAnswerCorrect ? 'correct' : ''}`}
                                    {...provided.droppableProps}
                                    ref={provided.innerRef}
                                >
                                    {notHelpfulItems.map(({id, text, image}, index) => (
                                        <Draggable key={id} draggableId={id} index={index}>
                                            {(provided: DraggableProvided) => (
                                                <li
                                                    ref={provided.innerRef}
                                                    {...provided.draggableProps}
                                                    {...provided.dragHandleProps}
                                                >
                                                    <div className="timeline-image">
                                                        <img src={image}
                                                             alt={`${text[language as keyof Texts]} image`}/>
                                                    </div>
                                                    <p className="text-left">{text[language as keyof Texts]}</p>
                                                </li>
                                            )}
                                        </Draggable>
                                    ))}
                                    {provided.placeholder}
                                </ul>
                            </>
                        )}
                    </StrictModeDroppable>
                </DragDropContext>
            </div>
        </>
    );
}

export default SymptomsDragNDropTest;
