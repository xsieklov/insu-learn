import {Dispatch, SetStateAction, useEffect, useState} from "react";
import Card from "./Card";
import {
    correctSelection,
    incorrectSelection,
    incorrectSymptomsCards,
    nextButton,
    symptomsCards,
    symptomsCardTitle
} from "../../public/lit/texts";
import {useRecoilValue} from "recoil";
import {languageAtom} from "../atoms/languageAtom";
import {Texts} from "../hooks/useAnimate";
import {useTranslate} from "../hooks/useTranslate";
import {useShuffle} from "../hooks/useShuffle";

interface SymptomsTestProps {
    handleNext: () => void,
    setScore: Dispatch<SetStateAction<number>>,
    score: number
}

interface Card {
    id: string;
    title: {
        EN: string;
        SK: string;
    };
    image: string;
}

const SymptomsCardTest = ({handleNext, setScore, score}: SymptomsTestProps) => {
    const language = useRecoilValue(languageAtom);
    const [selectedSymptoms, setSelectedSymptoms] = useState<string[]>([]);
    const nextButtonText = useTranslate(nextButton);
    const title = useTranslate(symptomsCardTitle);
    const correctSelectionText = useTranslate(correctSelection);
    const incorrectSelectionText = useTranslate(incorrectSelection);
    const [cards, setCards] = useState<Card[]>([]);

    useEffect(() => {
        setCards(useShuffle([...symptomsCards, ...incorrectSymptomsCards]));
    }, []);

    const handleCardClick = (symptomId: string) => {
        if (selectedSymptoms.includes(symptomId)) {
            setSelectedSymptoms((prev) => prev.filter((id) => id !== symptomId));
        } else {
            setSelectedSymptoms((prev) => [...prev, symptomId]);
        }
    };

    const isCorrect = () => {
        return (
            selectedSymptoms.length === symptomsCards.length &&
            symptomsCards.every((symptom) =>
                selectedSymptoms.includes(symptom.id)
            ))
            ;
    };

    return (
        <div>
            <p className="text-lg align-center block text-xl">{title}</p>
            {selectedSymptoms.length > 0 && (
                <p className={`my-4 ${isCorrect() ? 'text-green-500' : 'text-red-500'} text-xl`}>
                    {isCorrect()
                        ? `${correctSelectionText}`
                        : `${incorrectSelectionText}`}
                </p>
            )}
            {isCorrect() && (
                <button className="border-4 border-black font-primary bg-yellow-main text-xl"
                        onClick={() => {
                            setScore(score + 1);
                            handleNext()
                        }}>
                    {nextButtonText}
                </button>
            )}
            <div className="grid grid-cols-6 gap-4 my-4 mx-40 text-xl">
                {cards.map(({id, title, image}) => (
                    <Card
                        key={id}
                        image={image}
                        text={title[language as keyof Texts]}
                        alt={id}
                        isSelected={selectedSymptoms.includes(id)}
                        onClick={() => handleCardClick(id)}
                    />
                ))}
            </div>
        </div>
    );
}
export default SymptomsCardTest;
