interface ExplanationPopupTypes {
    explanation: string,
    className: string,
}
const ExplanationPopup = ({ explanation, className }: ExplanationPopupTypes) => {
    return (
        <div className={className}>
            <p>{explanation}</p>
        </div>
    );
};

export default ExplanationPopup;
