import Intro from './pages/Intro';
import {Route, Routes} from 'react-router-dom';
import {FC} from 'react';
import Navbar from './pages/Navbar';
import Chapters from './pages/Chapters';
import Insulin from "./pages/Insulin";
import HomePage from "./pages/HomePage";
import InsulinTest from "./pages/InsulinTest";
import Symptoms from "./pages/Symptoms";
import Treatment from "./pages/Treatment";
import SymptomsTest from "./pages/SymptomsTest";

const App: FC = () => {
  return (
    <>
      <div className="max-h-[100%] w-[100%] font-primary">
        <header>
          <Navbar />
        </header>
        <Routes>
          <Route path="/insu-learn" element={<HomePage />} index />
          <Route path="/insu-learn/chapters" element={<Chapters />} />
          <Route path="/insu-learn/intro" element={<Intro />} />
          <Route path="/insu-learn/insulin" element={<Insulin />} />
          <Route path="/insu-learn/insulinTest" element={<InsulinTest />} />
          <Route path="/insu-learn/symptomsTest" element={<SymptomsTest />} />
          <Route path="/insu-learn/symptoms" element={<Symptoms />} />
          <Route path="/insu-learn/treatment" element={<Treatment />} />
        </Routes>
      </div>
    </>
  );
};

export default App;
