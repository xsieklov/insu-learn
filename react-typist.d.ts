declare module '@banzai-inc/react-typist' {
  import { ComponentType } from 'react';

  export interface TypistProps {
    // Define the props used by Typist component
    cursor?: {
      hideWhenDone?: boolean;
    };
  }

  const Typist: ComponentType<TypistProps>;
  export default Typist;
}
