export default {
  content: ['./src/**/*.{js,jsx,ts,tsx}'],
  theme: {
    extend: {
      colors: {
        primary: {
          light: '#5599ff',
          main: '#3D348B',
          dark: '#aaccff',
        },
        beige: {
          light: '#e3c99f',
          main: '#d7b377',
          dark: '#ac8f5f',
        },
        red: {
          main: '#D35F5F',
        },
        blue: {
          main: '#D35F5F',
        },
        yellow: {
          main: '#EBE76C',
        },
        orange: {
          main: '#F0B86E',
        },
        purple: {
          main: '#836096',
        },
        gray: {
          main: '#d6d3d1',
          dark: '#a8a29e',
        },
        green: {
          main: '#5FD35F',
        }
      },
      fontFamily: {
        speech: ["'Inconsolata'", 'monospace'],
        primary: ["'Verdana'", 'sans-serif'],
      },
    },
  },
  plugins: [],
};
