import { defineConfig } from 'vite';
import react from '@vitejs/plugin-react';

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [react()],
  build: {
    outDir: 'public',
    base: '/insu-learn/',
    assetsInclude: ['animations/**', 'backgrounds/**', 'icons/**', 'lit/**', 'sounds/**'],
  },
  optimizeDeps: {
    include: ['**/*.html'],
  },
});
