## About
An interactive React application created to explain diabetes to children.

![](https://gitlab.fi.muni.cz/xsieklov/insu-learn/-/raw/main/public/gifs/clickable2.gif)

![](https://gitlab.fi.muni.cz/xsieklov/insu-learn/-/raw/main/public/gifs/clickable3.gif)

![](https://gitlab.fi.muni.cz/xsieklov/insu-learn/-/raw/main/public/gifs/image6.gif)

![](https://gitlab.fi.muni.cz/xsieklov/insu-learn/-/raw/main/public/gifs/image7.gif)
